require('dotenv').config();
const twit = require('twit');
const axios = require('axios');
const translate = require('translate');
const Fs = require('fs');
const Path = require('path');

let T = new twit({
    consumer_key:process.env.CONSUMER_KEY       ,
    consumer_secret: process.env.CONSUMER_SECRET,
    access_token: process.env.ACCESS_TOKEN,
    access_token_secret: process.env.ACCESS_TOKEN_SECRET,
})


async function download(url) {
    const path = Path.resolve(__dirname, 'files', 'image.jpg');
    const response =
        axios({
            method: 'GET',
            url: url,
            responseType: 'stream'
        }).then(function (response) {
            response.data.pipe(Fs.createWriteStream(path))
            return new Promise((resolve, reject) => {
                response.data.on('end', () => {
                    resolve()
                })
            });
        });
}


function asyncSleep(s) {
    return new Promise((resolve, reject) => {
        setTimeout(() => {
            resolve(true)
        }, 1000 * s || 1);
    })
}

function getSadPeople(sadText) {
    return new Promise(async (resolve, reject) => {
        let sadPeople = [];
        let twitterAction = await  T.get('search/tweets', { q: '"'+ sadText + '"', lang: 'es', count: 400, locale: 'es' }, function(err, data, response) {
            for(let i = 0; i < data.statuses.length; i++) {
                if( !data.statuses[i].in_reply_to_status_id && !data.statuses[i].retweeted && !data.statuses[i].text.startsWith('RT')) {
                    sadPeople.push({
                        'text': data.statuses[i].text,
                        'user': data.statuses[i].user.screen_name,
                        'tweetId': data.statuses[i].id_str
                    })
                }
            }
            resolve(sadPeople);
        });
    });
}

function answerSadPeople(data, cat) {
    let quote = [];
    axios.get('http://api.forismatic.com/api/1.0/?method=getQuote&&format=json&json=?&lang=en').then(function (response){
        if(response.data.quoteText != 'undefined') {
            translate(response.data.quoteText, { from: 'en', to: 'es', engine: 'google', key: process.env.KEY_TRASLATE }).then(text => {
                let autor = response.data.quoteAuthor;
                console.log(text +"  "+ autor);
                //console.log(getCat());
                //putTweet(text, autor);
                uploadImage(data.user, data.tweetId, text, autor);
                //T.post('statuses/update', { in_reply_to_status_id: data.tweetId, status: `@${data.user} ${text} ${autor}. ❤ 😉️` }, (err, data, response) => {})
            });
        }
    });
}

function putTweet(text, author) {
    T.post('statuses/update', { status: `${text} ${author}. ❤ 😉️` }, (err, data, response) => {})
}

function getCat() {
    return new Promise((resolve, reject) => {
        axios.get('https://api.thecatapi.com/v1/images/search').then(function (response) {
            resolve (response.data[0].url);
        });
    })
}

function uploadImage(user, tweetId, quote, author) {
    var filename = 'files/image.jpg'
    let b64content = Fs.readFileSync(filename, { encoding: 'base64' })
    T.post('media/upload', { media_data: b64content }, function (err, data, response) {
        let mediaIdStr = data.media_id_string
        let meta_params = { 'media_id': mediaIdStr }
        T.post('media/metadata/create', meta_params, (err, data, response) => {
            if (!err) {
                let params = {};
                if(quote == 'undefined') {
                    params = { 'status': `Dedicar tiempo, eso sí que es un regalo bonito. Defreds  ❤ 😉️`, media_ids: [mediaIdStr],  in_reply_to_status_id: tweetId }
                }else {
                    params = { 'status': `@${user} ${quote} ${author}. ❤ 😉️`, media_ids: [mediaIdStr],  in_reply_to_status_id: tweetId }
                }
                T.post('statuses/update', params, (err, data, response) => {
                    console.log('Image Uploaded!');
                });

            }
        });
    });
}

(async () => {
    let sadPeople =  await getSadPeople('dia de mierda');
    console.log(`Gente encontrada: ${sadPeople.length} tweets`);
   for(let i = 0; i < sadPeople.length; i++) {
       let cat = await getCat();
       await download(cat);
       answerSadPeople(sadPeople[i], cat);
       console.log(`+1 tweet to https://twitter.com/${sadPeople[i].user}/status/${sadPeople[i].tweetId}`);
       await asyncSleep(80);
   }
})();

